package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AckProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AckProducerApplication.class, args);
    }

}
