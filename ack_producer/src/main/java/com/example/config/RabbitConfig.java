package com.example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnsCallback {

    public static final String ACK_QUEUE_NAME = "ack_queue_name";
    public static final String ACK_EXCHANGE_NAME = "ack_exchange_name";

    private static final Logger logger = LoggerFactory.getLogger(RabbitConfig.class);

    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 把两个实例仍进来，并保存运行（PostConstruct）
     */
    @PostConstruct
    public void initRabbitTemplate() {
        rabbitTemplate.setReturnsCallback(this);
        rabbitTemplate.setConfirmCallback(this);
    }

    @Bean
    Queue ackQueue() {
        return new Queue(ACK_QUEUE_NAME,true,false,false);
    }

    @Bean
    DirectExchange ackExchange() {
        return new DirectExchange(ACK_EXCHANGE_NAME,true,false);
    }

    @Bean
    Binding ackBinding(){
        return BindingBuilder.bind(ackQueue()).to(ackExchange()).with(ACK_QUEUE_NAME);
    }


    /**
     * 消息成功到达交换机，触发该方法
     * correlationData：
     * b：ack true发送成功，false失败
     * s: 失败原因
     * @param correlationData
     * @param b
     * @param s
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean b, String s) {
        String id = correlationData.getId();
        if (b) {
            // 消息成功到达交换机
            logger.info("{}到达交换机",id);
        } else {
            // 失败
            logger.error("{}未到达交换机,原因：{}",id,s);
        }
    }

    /**
     * 消息没有路由到队列的回调函数，没到队列会触发，到达则不触发
     * @param returnedMessage
     */
    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        logger.error("{}消息未到达队列",returnedMessage.getMessage().getMessageProperties().getMessageId());
    }

}
