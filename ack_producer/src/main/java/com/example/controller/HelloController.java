package com.example.controller;

import com.example.config.RabbitConfig;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author 墨
 */
@RestController
public class HelloController {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/send")
    public void send() {
        rabbitTemplate.convertAndSend(RabbitConfig.ACK_EXCHANGE_NAME,RabbitConfig.ACK_QUEUE_NAME,"hello ack",new CorrelationData(UUID.randomUUID().toString()));
    }
}
