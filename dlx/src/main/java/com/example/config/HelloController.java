package com.example.config;

import com.example.congif.RabbitConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;

/**
 * @author 墨
 */
@RestController
public class HelloController {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @GetMapping("/send")
    public void send() {
        Message message = MessageBuilder.withBody("死信队列，你好".getBytes(StandardCharsets.UTF_8))
                // 10 秒未消费则进入死信队列（queue中配置死信队列）
                .setExpiration("10000").build();
        rabbitTemplate.send(RabbitConfig.DLX_GENERAL_EXCHANGE_NAME,RabbitConfig.DLX_GENERAL_NAME,message);
    }
}
