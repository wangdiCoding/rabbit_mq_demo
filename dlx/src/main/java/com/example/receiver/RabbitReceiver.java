package com.example.receiver;

import com.example.congif.RabbitDlxConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author 墨
 */
@Component
public class RabbitReceiver {

    @RabbitListener(queues = RabbitDlxConfig.DLX_QUEUE_NAME)
    public void receiver(String message) {
        System.out.println(message);
    }
}
