package com.example.congif;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig {
    public static final String DLX_GENERAL_NAME = "dlx_general_name";
    public static final String DLX_GENERAL_EXCHANGE_NAME = "dlx_general_exchange_name";

    @Bean
    Queue queue() {
        ConcurrentHashMap<String, Object> hashMap = new ConcurrentHashMap<>(4);
        //设置消息过期时间，消息过期之后吧，立马就会进入到死信队列中
        // 通过发送时生产者设置
        //hashMap.put("x-message-ttl", 0);

        //指定死信队列的交换机
        hashMap.put("x-dead-letter-exchange", RabbitDlxConfig.DLX_EXCHANGE_NAME);
        //指定死信队列路由的 key
        hashMap.put("x-dead-letter-routing-key", RabbitDlxConfig.DLX_QUEUE_NAME);
        return new Queue(DLX_GENERAL_NAME,true,false,false,hashMap);
    }

    @Bean
    DirectExchange exchange() {
        return new  DirectExchange(DLX_GENERAL_EXCHANGE_NAME,true,false);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange())
                .with(DLX_GENERAL_NAME);
    }
}
