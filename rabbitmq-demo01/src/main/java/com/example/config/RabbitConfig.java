package com.example.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig {
    public static final String QUEUE_NAME = "queue_name";

    @Bean
    Queue queue() {
        return new Queue(QUEUE_NAME,true,false,false);
    }

}
