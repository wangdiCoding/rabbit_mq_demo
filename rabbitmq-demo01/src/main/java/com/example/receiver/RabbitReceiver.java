package com.example.receiver;

import com.example.config.RabbitConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author 墨
 */
@Component
public class RabbitReceiver {

    @RabbitListener(queues = RabbitConfig.QUEUE_NAME)
    public void get(String message) {
        System.out.println("队列2："+message);
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_NAME,concurrency = "20")
    public void get2(String message) {
        System.out.println("队列1："+message);
    }
}
