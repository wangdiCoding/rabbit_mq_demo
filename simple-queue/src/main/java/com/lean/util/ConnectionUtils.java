package com.lean.util;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
* @program: rabbit_mq_demo
* @description: 连接工具
* @author: dwang
* @date: 2022-08-14 22:07
**/
public class ConnectionUtils {

    public static final String SIMPLE_QUEUE_NAME = "simple_queue";


    public static Connection getConnection() throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("127.0.0.1");

        factory.setPort(5672);

        factory.setVirtualHost("/");

        factory.setUsername("guest");
        factory.setPassword("guest");

        return factory.newConnection();
    }
}
