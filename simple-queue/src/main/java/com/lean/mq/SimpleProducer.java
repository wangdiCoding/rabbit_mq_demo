package com.lean.mq;

import com.lean.util.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @program: rabbit_mq_demo
 * @description: 简单模式生产者
 * @author: dwang
 * @date: 2022-08-15 19:45
 **/
public class SimpleProducer {



    public SimpleProducer() {
    }

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = ConnectionUtils.getConnection();

        Channel channel = connection.createChannel();

        channel.queueDeclare(ConnectionUtils.SIMPLE_QUEUE_NAME, false, false, false ,null);

        String msg = "simple msg";

        channel.basicPublish("",ConnectionUtils.SIMPLE_QUEUE_NAME, null, msg.getBytes());
        System.out.println("send msg is :" + msg);

        channel.close();

        connection.close();


    }
}
