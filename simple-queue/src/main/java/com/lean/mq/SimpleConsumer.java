package com.lean.mq;

import com.lean.util.ConnectionUtils;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @program: rabbit_mq_demo
 * @description: 简单消费消费者
 * @author: dwang
 * @date: 2022-08-15 19:53
 **/
public class SimpleConsumer {

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = ConnectionUtils.getConnection();

        Channel channel = connection.createChannel();

        channel.queueDeclare(ConnectionUtils.SIMPLE_QUEUE_NAME, false, false,false ,null);

        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String string = new String(body);
                //收到消息的回调
                System.out.println("consumer receive->" + string);
            }
        };

        channel.basicConsume(ConnectionUtils.SIMPLE_QUEUE_NAME, true, defaultConsumer);

    }
}
