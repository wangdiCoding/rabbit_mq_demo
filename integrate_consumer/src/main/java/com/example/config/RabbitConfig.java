package com.example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig {
    public static final String INTEGRATE_QUEUE_NAME = "integrate_queue_name";
    public static final String MESSAGE_QUEUE_NAME = "message_queue_name";
    public static final String INTEGRATE_EXCHANGE_NAME = "integrate_exchange_name";

    private static final Logger logger = LoggerFactory.getLogger(RabbitConfig.class);

    @Bean
    Queue integrateQueue() {
        ConcurrentHashMap<String, Object> hashMap = new ConcurrentHashMap<>(4);
        // 指定死信队列的交换机
        hashMap.put("x-dead-letter-exchange", RabbitDlxConfig.DLX_INTEGRATE_EXCHANGE_NAME);
        // 指定死信队列路由的 key
        hashMap.put("x-dead-letter-routing-key", RabbitDlxConfig.DLX_INTEGRATE_QUEUE_NAME);
        return new Queue(INTEGRATE_QUEUE_NAME, true, false, false, hashMap);
    }

    @Bean
    Queue messageQueue() {
        ConcurrentHashMap<String, Object> hashMap = new ConcurrentHashMap<>(4);
        //指定死信队列的交换机
        hashMap.put("x-dead-letter-exchange", RabbitDlxConfig.DLX_MESSAGE_EXCHANGE_NAME);
        //指定死信队列路由的 key
        hashMap.put("x-dead-letter-routing-key", RabbitDlxConfig.DLX_MESSAGE_QUEUE_NAME);
        return new Queue(MESSAGE_QUEUE_NAME, true, false, false, hashMap);
    }

    @Bean
    TopicExchange integrateExchange() {
        return new TopicExchange(INTEGRATE_EXCHANGE_NAME, true, false);
    }

    @Bean
    Binding integrateBinding() {
        return BindingBuilder.bind(integrateQueue()).to(integrateExchange()).with("#.queue.#");
    }

    @Bean
    Binding messageBinding() {
        return BindingBuilder.bind(messageQueue()).to(integrateExchange()).with("message.queue.#");
    }
}
