package com.example.receiver;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.GetResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * 拉模式手动确认，测试方法中测试
 * @author 墨
 */
@Configuration
public class RabbitReceiver {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    private static final Logger logger = LoggerFactory.getLogger(RabbitReceiver.class);

    public void getIntegrate(String queueName) {
        long deliveryTag = 0;
        Channel channel = rabbitTemplate.getConnectionFactory().createConnection().createChannel(false);
        try {
            GetResponse response = channel.basicGet(queueName, false);
            deliveryTag = response.getEnvelope().getDeliveryTag();
            byte[] body = response.getBody();
            logger.info("收到的消息是：{}",new String(body));
            /**
             * multiple: true 之前的所有消息都被消费，如果false则只确认当前消息被消费
             */
            channel.basicAck(deliveryTag,true);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("{}ack错误，原因：{},错误消息：{}",deliveryTag,e.getCause(),e.getMessage());

            try {
                /**
                 * requeue:true 回到队列中重新消费，false：如果配置了死信队列则进入死信队列，否则丢失
                 */
                channel.basicNack(deliveryTag,true,false);
            } catch (IOException ex) {
                ex.printStackTrace();
                logger.error("nack错误，原因：{},错误消息:{}",ex.getCause(),ex.getMessage());
            }
        }
    }
}
