package com.example.receiver;

import com.example.config.RabbitDlxConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 监听死信队列
 * @author 墨
 */
@Component
public class DlxRabbitReceiver {
    private static final Logger logger = LoggerFactory.getLogger(DlxRabbitReceiver.class);

    @RabbitListener(queues = RabbitDlxConfig.DLX_INTEGRATE_QUEUE_NAME)
    public void dlxIntegrateMessage(String message) {
        logger.info("进入Integrate死信消息,消息：{}",message);
        System.out.println("其他后续操作");
    }

    @RabbitListener(queues = RabbitDlxConfig.DLX_MESSAGE_QUEUE_NAME)
    public void dlxMessage(String message) {
        logger.info("进入Message死信消息,消息：{}",message);
        System.out.println("其他后续操作");
    }
}
