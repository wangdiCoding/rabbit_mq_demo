package com.example;

import com.example.config.RabbitConfig;
import com.example.receiver.RabbitReceiver;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class IntegrateConsumerApplicationTests {
    @Autowired
    private RabbitReceiver rabbitReceiver;

    private static final String INTEGRATE_QUEUE_NAME = RabbitConfig.INTEGRATE_QUEUE_NAME;
    private static final String MESSAGE_QUEUE_NAME = RabbitConfig.MESSAGE_QUEUE_NAME;

    /**
     * 生产消息后手动拉取一条消息，否则消息过期后进入死信队列
     */
    @Test
    void integrateQueueTest() {
        rabbitReceiver.getIntegrate(INTEGRATE_QUEUE_NAME);
    }

    @Test
    void messageQueueTest() {
        rabbitReceiver.getIntegrate(MESSAGE_QUEUE_NAME);
    }

}
