package com.example.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig {
    public static final String TTL_QUEUE_NAME = "ttl_queue_name";
    public static final String TTL_EXCHANGE_NAME = "ttl_exchange_name";

    @Bean
    Queue queue() {
        ConcurrentHashMap<String, Object> hashMap = new ConcurrentHashMap<>(4);
        // 过期时间设置10秒，如果没有被消费就过期
        hashMap.put("x-message-ttl", 10000);
        return new Queue(TTL_QUEUE_NAME,true,false,false,hashMap);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(TTL_EXCHANGE_NAME,true,false);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange()).with(TTL_QUEUE_NAME);
    }
}
