package com.example.receiver;

import com.example.config.RabbitConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author 墨
 */
@Component
public class RabbitReceiver {

    @RabbitListener(queues = RabbitConfig.TOPIC_XIAOMI_QUEUE)
    public void xiaomi(String message) {
        System.out.println("小米订阅："+message);
    }

    @RabbitListener(queues = RabbitConfig.TOPIC_HUAWEI_QUEUE)
    public void huawei(String message) {
        System.out.println("华为订阅："+message);
    }

    @RabbitListener(queues = RabbitConfig.TOPIC_PHONE_QUEUE)
    public void phone(String message) {
        System.out.println("手机订阅："+message);
    }

}
