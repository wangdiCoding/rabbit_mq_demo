package com.example.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig {
    public static final String TOPIC_HUAWEI_QUEUE = "huawei_queue";
    public static final String TOPIC_XIAOMI_QUEUE = "xiaomi_queue";
    public static final String TOPIC_PHONE_QUEUE = "topic_phone_queue";

    public static final String TOPIC_EXCHANGE_NAME = "topic_exchange_name";

    @Bean
    Queue xiaomi() {
        return new Queue(TOPIC_XIAOMI_QUEUE, true, false, false);
    }

    @Bean
    Queue huawei() {
        return new Queue(TOPIC_HUAWEI_QUEUE, true, false, false);
    }

    @Bean
    Queue phone() {
        return new Queue(TOPIC_PHONE_QUEUE, true, false, false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(TOPIC_EXCHANGE_NAME, true, false);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(xiaomi()).to(exchange()).with("xiaomi.#");
    }

    @Bean
    Binding binding2() {
        return BindingBuilder.bind(huawei()).to(exchange()).with("huawei.#");
    }

    @Bean
    Binding binding3() {
        return BindingBuilder.bind(phone()).to(exchange()).with("#.phone.#");
    }

}
