package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicsProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TopicsProducerApplication.class, args);
    }

}
