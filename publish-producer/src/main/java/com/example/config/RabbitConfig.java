package com.example.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig {
    public static final String EXCHANGE_NAME = "exchange_name";
    public static final String QUEUE_NAME = "queue_name";
    public static final String QUEUE_NAME_1 = "queue_name1";

    @Bean
    Queue queue1() {
        return new Queue(QUEUE_NAME,true,false,false);
    }

    @Bean
    Queue queue2() {
        return new Queue(QUEUE_NAME_1,true,false,false);
    }

    @Bean
    FanoutExchange exchange() {
        return new FanoutExchange(EXCHANGE_NAME,true,false);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(queue1()).to(exchange());
    }

    @Bean
    Binding binding1() {
        return BindingBuilder.bind(queue2()).to(exchange());
    }
}
