package com.example.receiver.push;

import com.example.config.RabbitConfig;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * 推模式手动确认
 * @author 墨
 */
// 如果要测试拉模式请注释@Configuration
@Configuration
public class RabbitReceiver {
    private static final Logger logger = LoggerFactory.getLogger(RabbitReceiver.class);

    @RabbitListener(queues = RabbitConfig.ACK_QUEUE_NAME)
    public void get(Message message, Channel channel) {
        // 获取一个消息的标记
        long deliveryTag = message.getMessageProperties().getDeliveryTag();

        try {
            // 开始消费的消息
            byte[] body = message.getBody();
            String s = new String(body);
            logger.info("handleMessage",s);

            // multiple:false,仅仅确认当前消息，true则确认之前所有的消息都消费成功
            channel.basicAck(deliveryTag,false);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                // requeue:true，消费失败，重新回到队列中共下次消费，如果是false:当包含死信队列时，进入死信队列，如果没有死信队列则丢失
                channel.basicNack(deliveryTag,false,true);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
