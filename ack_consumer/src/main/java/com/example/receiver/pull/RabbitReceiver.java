package com.example.receiver.pull;

import com.example.config.RabbitConfig;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.GetResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * 拉模式手动确认,从测试方法中测试
 *
 * @author 墨
 */
//@Configuration
public class RabbitReceiver {
    @Autowired
    RabbitTemplate rabbitTemplate;

    private static final Logger logger = LoggerFactory.getLogger(RabbitReceiver.class);

    public void get() {
        long deliveryTag = 0;
        // transaction：false 不开启事务
        Channel channel = rabbitTemplate.getConnectionFactory().createConnection().createChannel(false);
        try {
            GetResponse get = channel.basicGet(RabbitConfig.ACK_QUEUE_NAME, false);
            deliveryTag = get.getEnvelope().getDeliveryTag();
            byte[] body = get.getBody();
            String s = new String(body);
            logger.info("收到的内容是：{}", s);
            channel.basicAck(deliveryTag, true);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("ack错误");

            try {
                // requeue:true 时回到队列中重新消费，false：如果配了死信队列就进死信队列，如果没有就丢失
                channel.basicNack(deliveryTag, false, true);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
