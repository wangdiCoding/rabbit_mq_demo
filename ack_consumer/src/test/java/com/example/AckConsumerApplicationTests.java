package com.example;

import com.example.receiver.pull.RabbitReceiver;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AckConsumerApplicationTests {
    @Autowired
    private RabbitReceiver rabbitReceiver;

    @Test
    public void get() {
        rabbitReceiver.get();
    }

    @Test
    void contextLoads() {
    }

}
