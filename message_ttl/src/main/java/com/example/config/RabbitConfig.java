package com.example.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig {
    public static final String TTL_MESSAGE_QUEUE_NAME = "ttl_message_queue_name";
    public static final String TTL_MESSAGE_EXCHANGE_NAME = "ttl_message_exchange_name";

    @Bean
    Queue queue() {
        return new Queue(TTL_MESSAGE_QUEUE_NAME,true,false,false);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(TTL_MESSAGE_EXCHANGE_NAME,true,false);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange()).with(TTL_MESSAGE_QUEUE_NAME);
    }
}
