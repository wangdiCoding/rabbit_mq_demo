package com.example.controller;

import com.example.config.RabbitConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;

/**
 * @author 墨
 */
@RestController
public class HelloController {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/send")
    public void send() {
        Message build = MessageBuilder.withBody("hello".getBytes(StandardCharsets.UTF_8))
                // 设置过期时间
                .setExpiration("10000").build();
        rabbitTemplate.send(RabbitConfig.TTL_MESSAGE_EXCHANGE_NAME,RabbitConfig.TTL_MESSAGE_QUEUE_NAME,build);
    }
}
