package com.example.receiver;

import com.example.config.RabbitConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author 墨
 */
@Component
public class RabbitReceiver {

    @RabbitListener(queues = RabbitConfig.QUEUE_NAME)
    public void get(String message) {
        System.out.println("第一个队列："+message);
    }

    @RabbitListener(queues = RabbitConfig.QUEUE_NAME_1)
    public void get2(String message) {
        System.out.println("第二个队列："+message);
    }

}
