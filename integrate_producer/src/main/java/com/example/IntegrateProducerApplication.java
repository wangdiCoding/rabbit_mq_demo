package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 墨
 */
@SpringBootApplication
public class IntegrateProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegrateProducerApplication.class, args);
    }

}
