package com.example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 死信队列
 * @author 墨
 */
@Configuration
public class RabbitDlxConfig {
    private static final Logger logger = LoggerFactory.getLogger(RabbitDlxConfig.class);

    public static final String DLX_INTEGRATE_QUEUE_NAME = "dlx_integrate_queue_name";
    public static final String DLX_INTEGRATE_EXCHANGE_NAME = "dlx_integrate_exchange_name";

    public static final String DLX_MESSAGE_QUEUE_NAME = "dlx_message_queue_name";
    public static final String DLX_MESSAGE_EXCHANGE_NAME = "dlx_message_exchange_name";

    @Bean
    Queue dlxIntegrateQueue() {
        return new Queue(DLX_INTEGRATE_QUEUE_NAME,true,false,false);
    }

    @Bean
    Queue dlxMessageQueue() {
        return new Queue(DLX_MESSAGE_QUEUE_NAME,true,false,false);
    }

    @Bean
    DirectExchange dlxIntegrateExchange() {
        return new DirectExchange(DLX_INTEGRATE_EXCHANGE_NAME,true,false);
    }

    @Bean
    DirectExchange dlxMessageExchange() {
        return new DirectExchange(DLX_MESSAGE_EXCHANGE_NAME,true,false);
    }

    @Bean
    Binding dlxIntegrateBinding() {
        return BindingBuilder.bind(dlxIntegrateQueue()).to(dlxIntegrateExchange()).with(DLX_INTEGRATE_QUEUE_NAME);
    }

    @Bean
    Binding dlxMessageBinding() {
        return BindingBuilder.bind(dlxMessageQueue()).to(dlxMessageExchange()).with(DLX_MESSAGE_QUEUE_NAME);
    }
}
