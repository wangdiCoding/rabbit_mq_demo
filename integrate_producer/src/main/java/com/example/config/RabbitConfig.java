package com.example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 墨
 */
@Configuration
public class RabbitConfig implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnsCallback {
    public static final String INTEGRATE_QUEUE_NAME = "integrate_queue_name";
    public static final String MESSAGE_QUEUE_NAME = "message_queue_name";

    public static final String INTEGRATE_EXCHANGE_NAME = "integrate_exchange_name";

    private static final Logger logger = LoggerFactory.getLogger(RabbitConfig.class);

    @Bean
    Queue integrateQueue() {
        ConcurrentHashMap<String, Object> hashMap = new ConcurrentHashMap<>(4);
        // 指定死信队列的交换机
        hashMap.put("x-dead-letter-exchange", RabbitDlxConfig.DLX_INTEGRATE_EXCHANGE_NAME);
        // 指定死信队列路由的 key
        hashMap.put("x-dead-letter-routing-key", RabbitDlxConfig.DLX_INTEGRATE_QUEUE_NAME);
        return new Queue(INTEGRATE_QUEUE_NAME, true, false, false,hashMap);
    }

    @Bean
    Queue messageQueue() {
        ConcurrentHashMap<String, Object> hashMap = new ConcurrentHashMap<>(4);
        //指定死信队列的交换机
        hashMap.put("x-dead-letter-exchange", RabbitDlxConfig.DLX_MESSAGE_EXCHANGE_NAME);
        //指定死信队列路由的 key
        hashMap.put("x-dead-letter-routing-key", RabbitDlxConfig.DLX_MESSAGE_QUEUE_NAME);
        return new Queue(MESSAGE_QUEUE_NAME, true, false, false,hashMap);
    }

    @Bean
    TopicExchange integrateExchange() {
        return new TopicExchange(INTEGRATE_EXCHANGE_NAME, true, false);
    }

    @Bean
    Binding integrateBinding() {
        return BindingBuilder.bind(integrateQueue()).to(integrateExchange()).with("#.queue.#");
    }

    @Bean
    Binding messageBinding() {
        return BindingBuilder.bind(messageQueue()).to(integrateExchange()).with("message.queue.#");
    }

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void initRabbitTemplate() {
        rabbitTemplate.setConfirmCallback(this::confirm);
        rabbitTemplate.setReturnsCallback(this::returnedMessage);
    }

    /**
     * 消息成功到达交换机，触发该方法
     *
     * @param correlationData 消息内容，主要获取id
     * @param ack             true: 发送成功，false：失败
     * @param cause           原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        String id = correlationData.getId();
        if (ack) {
            logger.info("{}到达交换机", id);
        } else {
            logger.error("{}未到达交换机,原因：{}", id, cause);
        }
    }

    /**
     * 消息没有路由到队列的回调函数，没到队列会触发，到达则不触发
     *
     * @param returned
     */
    @Override
    public void returnedMessage(ReturnedMessage returned) {
        logger.error("{}消息未到达队列", returned.getMessage().getMessageProperties().getMessageId());
    }
}
